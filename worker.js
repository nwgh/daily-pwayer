self.addEventListener('install', (e) => {
    e.waitUntil(
        caches.open('prayer').then((cache) => cache.addAll([
            '/daily-pwayer/',
            '/daily-pwayer/index.html',
            '/daily-pwayer/index.js',
            '/daily-pwayer/writ.css',
            '/daily-pwayer/prayer.css',
            '/daily-pwayer/offline.html',
            // TODO - more here
        ])),
    );
});

self.addEventListener('fetch', (e) => {
    e.respondWith(
        caches.match(e.request).then((response) => {
            if (response !== undefined) {
                return response;
            }

            return fetch(e.request).then((fresponse) => {
                // Safety option in case we forgot to pre-populate the cache
                let clone = fresponse.clone();
                caches.open('prayer').then((cache) => {
                    cache.put(e.request, clone);
                });
                return fresponse;
            }).catch(() => {
                // Super safety option in case we typoed a URI or are offline
                return caches.match('/daily-pwayer/offline.html');
            });
        })
    );
});
